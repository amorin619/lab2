package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature{

	public Kelvin(float t)
	{
		super(t);
	}

	public String toString()
	{
		String ret = Float.toString(getValue()) + " K";
		return ret;
	}
	@Override
	public Temperature toCelsius() {
		float val = getValue();
		float conversion = val - 273.15f;
		Temperature retVal = new Celsius(conversion);
		return retVal;
	}
	@Override
	public Temperature toFahrenheit() {
		float val = getValue();
		float conversion = val*9.0f/5 - 459.67f;
		Temperature retVal = new Fahrenheit(conversion);
		return retVal;
	}

	@Override
	public Temperature toKelvin() {
		Temperature retVal = new Kelvin(getValue());
		return retVal;
	}
}
