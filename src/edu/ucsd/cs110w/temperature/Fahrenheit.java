package edu.ucsd.cs110w.temperature;
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		String ret = Float.toString(getValue()) + " F";
		return ret;
	}
	@Override
	public Temperature toCelsius() {
		float val = getValue();
		float conversion = (val-32)*5.0f/9;
		Temperature retVal = new Celsius(conversion);
		return retVal;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature retVal = new Fahrenheit(getValue());
		return retVal;
	}
	@Override
	public Temperature toKelvin() {
		float val = getValue();
		float conversion = (val + 459.67f)*5.0f/9;
		Temperature retVal = new Kelvin(conversion);
		return retVal;
	}
}
